export const SET_ENV = 'env/SET_ENV'
const initialState = window.env

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_ENV:
            return {...state, ...action.env}
        default:
            return state
    }
}

export const setEnv = (env) => {
    return dispatch => {
        dispatch({
            type: SET_ENV,
            env: env
        })
    }
}