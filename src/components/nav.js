import React, {Component} from 'react';
import _ from 'lodash'

/**
 *
 * @param {string} [id] - dom element #id
 * @param {object} [menu] - menu info
 * @param {string} [menuIndex] - now menu index
 * @param {function} [onChange] - handle tab change
 * @param {function} [handleAdd] - handle add while click +
 */

class Nav extends Component {//object

    // AddNavTab() {
    //     const {handleAdd} = this.props
    //     handleAdd();
    // }
    //
    // DeleteNavTab(key) {
    //     const {handleDelete} = this.props
    //     handleDelete(key);
    // }

    checkActive(index) {
        if (index === this.props.menuIndex) {
            return 'nav-link active'
        }
        return 'nav-link'
    }

    switchTab(index) {
        const {onChange} = this.props
        onChange(index)
    }

    renderContent(){
        const {menu, menuIndex} = this.props
        return menu[menuIndex].content
    }

    render() {//TODO remove
        const {menu, id} = this.props
        return (
            <div id={id}>
                <ul className="nav nav-tabs">
                    {_.map(menu, ({label}, key) =>
                        <li className="nav-item" key={key}>
                            <div className="relative">
                                <a className={this.checkActive(key)} href="javascript:void(0);" onClick={(e) => this.switchTab(key)}>{label}</a>
                                {/*<button type="button" className="custom btn btn-danger deleteButton" style={{position: 'absolute', right: '0px', top: '0px'}}*/}
                                        {/*onClick={() => {*/}
                                            {/*this.DeleteNavTab(key)*/}
                                        {/*}}>&#8722; </button>*/}
                            </div>
                        </li>
                    )}
                    {/*<li className="nav-item">*/}
                        {/*<a className="nav-link " href="javascript:void(0);" onClick={(e) => this.AddNavTab()}>+</a>*/}
                    {/*</li>*/}
                </ul>
                <br/>
                <div>
                    {this.renderContent()}
                </div>
            </div>)
    }
}
export default Nav