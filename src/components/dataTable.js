import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import im from 'object-path-immutable';
import PropTypes from 'prop-types';
import cx from 'classnames';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import {
  faPlus,
  faSortAlphaDown,
  faSortAlphaUp
} from '@fortawesome/fontawesome-free-solid';

import * as Common from './utils/common';
import Input from './input/input';

class DataTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sorts: {}
    };
  }

  editObjectKey(object, oldkey, newkey) {
    const { handleChange } = this.props;
    let newObject = Common.renameObjectKey(object, oldkey, newkey);
    handleChange(newObject);
  }

  addButton() {
    const { addable } = this.props;
    if (addable)
      return (
        <a
          className="cursor-pointer"
          onClick={() => {
            this.handleAdd();
          }}
        >
          <FontAwesomeIcon icon={faPlus} size={'sm'} />
        </a>
      );
  }

  handleAdd() {
    const { handleChange, emptyRow = {} } = this.props;
    let { rawData } = this.props;
    if (!rawData) rawData = {};
    if (_.isArray(rawData)) rawData = Common.handleArrayAdd(rawData, emptyRow);
    else rawData = Common.handleObjectAdd(rawData, 'newOne', emptyRow);
    handleChange(rawData);
  }

  handleDelete(key) {
    const { handleChange } = this.props;
    let { rawData } = this.props;
    if (_.isArray(rawData)) Common.handleArrayDelete(rawData, key);
    else Common.handleObjectDelete(rawData, key);
    handleChange(rawData);
  }

  renderEditor(value, rid, field, fieldsKey) {
    const { rawData, handleChange } = this.props;
    let { editor, title, props } = field;

    props = {
      ...field.props,
      id: `${fieldsKey}_${title}`,
      value,
      onChange: value => {
        _.set(rawData[rid], fieldsKey, value);
        handleChange(rawData);
      }
    };
    return React.createElement(editor, props);
  }

  renderSortButton(key) {
    const { sorts = {} } = this.state;
    const sort = _.get(sorts, key, 'DESC');
    return (
      <a
        className="cursor-pointer sort-button"
        onClick={() => {
          this.onSort(key);
        }}
      >
        <FontAwesomeIcon
          icon={sort === 'DESC' ? faSortAlphaDown : faSortAlphaUp}
          size={'sm'}
        />
      </a>
    );
  }

  onSort(key) {
    const { sorts = {} } = this.state;
    const { onSort } = this.props;
    const sort = _.get(sorts, key, 'DESC');
    const sortTo = sort === 'DESC' ? 'ASC' : 'DESC';
    this.setState({
      sorts: im.set(sorts, key, sortTo)
    });
    onSort(key, sortTo);
  }

  renderField(field, row, fieldsKey, rowKey) {
    let value = _.get(row, fieldsKey, null);
    if (field.formatter)
      return <div>{field.formatter(value, row, rowKey)}</div>;
    if (typeof value === 'boolean') value = value.toString();
    let content = null;
    if (field.editor)
      content = this.renderEditor(value, rowKey, field, fieldsKey);
    else if (field.format) {
      switch (field.format) {
        case 'date':
          content = moment(value).format('YYYY-MM-DD');
          break;
        case 'dateTime':
          content = moment(value).format('YYYY-MM-DD HH:mm:ss');
          break;
      }
    } else content = value !== null ? value : '';
    return <div>{content}</div>;
  }

  render() {
    const {
      id,
      rawData,
      fields,
      className = 'table-striped table-bordered',
      theadClassName = 'thead-dark',
      keyTitle,
      addable,
      deletable
    } = this.props;
    let totalFields = _.size(fields);
    if (keyTitle) totalFields++;
    if (deletable) totalFields++;
    return (
      <div>
        <table id={'dataTable-' + id} className={cx('table', className)}>
          <thead className={theadClassName}>
            <tr>
              {deletable ? <th>#</th> : null}
              {keyTitle ? <th>{keyTitle}</th> : null}
              {_.map(fields, (field, key) => {
                return (
                  <th key={key}>
                    {field.title}
                    {field.sortable
                      ? this.renderSortButton(
                          field.sortKey ? field.sortKey : key
                        )
                      : null}
                  </th>
                );
              })}
            </tr>
          </thead>
          <tbody>
            {_.isEmpty(rawData) ? (
              <tr>
                <td colSpan={totalFields} className="tac">
                  無資料
                </td>
              </tr>
            ) : null}
            {_.map(rawData, (row, rowKey) => {
              return (
                <tr key={rowKey}>
                  {deletable ? (
                    <td>
                      <div>
                        <a
                          className="cursor-pointer"
                          onClick={() => {
                            this.handleDelete(rowKey);
                          }}
                        >
                          X
                        </a>
                      </div>
                    </td>
                  ) : null}
                  {keyTitle ? (
                    <td>
                      {addable ? (
                        <Input
                          id={`${id}_${rowKey}_Name`}
                          placeholder="Name"
                          value={rowKey}
                          handleChange={value => {
                            this.editObjectKey(rawData, rowKey, value);
                          }}
                          validation={['required']}
                          tooltipPlacement="bottom"
                        />
                      ) : (
                        rowKey
                      )}
                    </td>
                  ) : null}
                  {_.map(fields, (field, fieldsKey) => {
                    return (
                      <td key={fieldsKey}>
                        {this.renderField(field, row, fieldsKey, rowKey)}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
            {addable ? (
              <tr>
                <td colSpan={totalFields} className="tac">
                  {this.addButton()}
                </td>
              </tr>
            ) : null}
          </tbody>
        </table>
      </div>
    );
  }
}

export default DataTable;

DataTable.propTypes = {
  addable: PropTypes.bool,
  className: PropTypes.string,
  deletable: PropTypes.bool,
  fields: PropTypes.object.isRequired,
  handleChange: PropTypes.func,
  id: PropTypes.string,
  keyTitle: PropTypes.string,
  keyTitleCol: PropTypes.number,
  rawData: PropTypes.any.isRequired
};
