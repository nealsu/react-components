import PropTypes from 'prop-types';
import React from 'react';
import _ from 'lodash';
import im from 'object-path-immutable';
import { FormInput, FormDateRange } from './input';
import cx from 'classnames';

const InputType = {
  INPUT: 'Input',
  DATE_RANGE: 'DateRange',
  TEXT_AREA: 'TextArea',
  DROP_DOWN_LIST: 'DropDownList',
  CHECK_BOX: 'CheckBox'
};
class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // formData: this.props.data
    };
  }

  renderInner(field, key) {
    const { data } = this.props;
    const value = _.get(data, key, '');
    if (_.has(field, 'editor')) {
      const props = {
        id: 'input-' + key,
        value,
        onChange: value => {
          this.onChange(key, value);
        },
        ...field.props
      };
      return (
        <div id={key} className="form-group">
          <label htmlFor={key}>{field.title}</label>
          {React.createElement(field.editor, props)}
        </div>
      );
    }
    switch (field.inputType) {
      case InputType.INPUT:
      default:
        return (
          <FormInput
            id={key}
            label={field.title}
            placeholder={field.placeholder ? field.placeholder : field.title}
            value={value}
            onChange={value => {
              this.onChange(key, value);
            }}
          />
        );
      case InputType.DATE_RANGE:
        return (
          <FormDateRange
            id={key}
            label={field.title}
            value={value}
            onChange={value => {
              this.onChange(key, value);
            }}
          />
        );
    }
  }
  onChange(key, value) {
    const { onChange } = this.props;
    const { data } = this.props;
    let tempFormData = im.set(data, key, value);
    // this.setState({data: tempFormData})
    onChange(tempFormData);
  }

  render() {
    const { fields, columnCount = 1 } = this.props;
    const bootstrapCol = 12 / columnCount;
    return (
      <form className={cx(columnCount !== 1 ? 'row' : '')}>
        {_.map(fields, (field, key) => {
          return (
            <div
              key={key}
              className={cx(columnCount !== 1 ? 'col-' + bootstrapCol : '')}
            >
              {this.renderInner(field, key)}
            </div>
          );
        })}
      </form>
    );
  }
}

export { Form, InputType };

Form.propTypes = {
  data: PropTypes.object.isRequired,
  fields: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired
};
