import PropTypes from 'prop-types';
import React from 'react';
import ReactDom from 'react-dom';
import Loader from 'react-loader-spinner';
class LoaderSpinner extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      spinning: false
    };
  }

  start() {
    this.setState({ spinning: true });
  }
  stop() {
    this.setState({ spinning: false });
  }
  render() {
    if (!this.state.spinning) return null;
    return (
      <div
        style={{
          position: 'fixed',
          bottom: '0',
          left: '0',
          right: '0',
          top: '0',
          opacity: '0.5',
          backgroundColor: '#000',
          zIndex: '9999'
        }}
      >
        <div
          style={{
            position: 'fixed',
            left: '50%',
            top: '50%',
            margin: '-25px',
            zIndex: '100'
          }}
        >
          <Loader type={this.props.type} color="#FFF" height="50" width="50" />
        </div>
      </div>
    );
  }
}

export default { start, stop };

/**
 *
 * @type {{type: *}}//Audio Ball-Triangle Bars Circles Grid Hearts Oval Puff Rings TailSpin ThreeDots
 */
LoaderSpinner.propTypes = {
  type: PropTypes.string.isRequired
};

LoaderSpinner.defaultProps = {
  type: 'TailSpin'
};

let globalProgress = null;
export function initial() {
  let node = document.createElement('DIV');
  node.id = 'Loader-Spinner';
  document.body.appendChild(node);
  ReactDom.render(
    <LoaderSpinner
      ref={ref => {
        globalProgress = ref;
        // globalProgress.start()
      }}
    />,
    document.getElementById('Loader-Spinner')
  );
}
function start(args) {
  if (globalProgress) {
    globalProgress.start(args);
  }
}
function stop(delay = 0) {
  if (globalProgress) {
    setTimeout(() => {
      globalProgress.stop();
    }, delay);
  }
}
