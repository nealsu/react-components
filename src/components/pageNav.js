import React from 'react';
import cx from 'classnames';
import _ from 'lodash';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import {
  faAngleLeft,
  faAngleRight
} from '@fortawesome/fontawesome-free-solid/index';

/**
 * @param {number} [pages] - Total number of pages
 * @param {number} [current=1] - Current (highlighted) page number
 * @param {number} [thumbnails=9] - Maximum number of thumbnails to display
 * @param {string} [className] - Classname for the container
 * @param {function} onChange - Callback function when from/to is changed. <br> Required when value prop is supplied
 * @param {number} onChange.page - current selected page
 */
const defaultButtonClass = 'btn-secondary';
class PageNav extends React.Component {
  // propTypes: {
  //     pages: React.PropTypes.number,
  //     current: React.PropTypes.number,
  //     thumbnails: React.PropTypes.number,
  //     className: React.PropTypes.string,
  //     onChange: React.PropTypes.func.isRequired
  // }
  // getDefaultProps() {
  //     return {
  //         pages: null,
  //         current: 1,
  //         thumbnails: 9
  //     }
  // }
  gotoPage(page) {
    let { onChange } = this.props;
    onChange(page);
  }

  renderThumb(page, key) {
    let { current, buttonClassName } = this.props;
    return (
      <button
        key={key}
        className={cx(
          'thumb btn',
          { active: current === page },
          buttonClassName ? buttonClassName : defaultButtonClass
        )}
        disabled={!page}
        onClick={this.gotoPage.bind(this, page)}
      >
        {page || '...'}
      </button>
    );
  }

  render() {
    let {
      thumbnails = 9,
      current,
      pages,
      className,
      buttonClassName
    } = this.props;

    if (!pages) {
      return (
        <div className={cx('btn-group', className)} role="group" aria-label="">
          <button
            className={cx(
              'thumb btn',
              buttonClassName ? buttonClassName : defaultButtonClass
            )}
            disabled={true}
          >
            <FontAwesomeIcon icon={faAngleLeft} size={'sm'} />
          </button>
          <button
            className={cx(
              'thumb btn',
              buttonClassName ? buttonClassName : defaultButtonClass
            )}
            disabled={true}
          >
            0
          </button>
          <button
            className={cx(
              'thumb btn ',
              buttonClassName ? buttonClassName : defaultButtonClass
            )}
            disabled={true}
          >
            <FontAwesomeIcon icon={faAngleRight} size={'sm'} />
          </button>
        </div>
      );
    }

    // let endThumbs = Math.floor(thumbnails/4);
    let endThumbs = 2; // display 2 at both ends
    let midThumbs = thumbnails - endThumbs * 2 - 2;
    let list = [];

    let midStart = Math.max(current - Math.floor(midThumbs / 2), endThumbs + 1);
    let midEnd = midStart + midThumbs - 1;
    let lastSkipped = false;

    if (midEnd >= pages - endThumbs) {
      midStart = Math.max(
        endThumbs + 1,
        midStart - (midEnd - (pages - endThumbs))
      );
      midEnd = pages - endThumbs;
      midStart--;
    }

    if (midStart === endThumbs + 1) {
      midEnd++;
    }

    if (midStart === endThumbs + 2) {
      midStart--;
    }
    if (midEnd === pages - endThumbs - 1) {
      midEnd++;
    }

    _.forEach(_.range(1, pages + 1), i => {
      if (
        i <= endThumbs ||
        i > pages - endThumbs ||
        (i >= midStart && i <= midEnd)
      ) {
        list.push(this.renderThumb(i, i));
        lastSkipped = false;
      } else {
        if (!lastSkipped) {
          list.push(this.renderThumb(null, i));
          lastSkipped = true;
        }
      }
    });

    return (
      <div className={cx('btn-group', className)} role="group" aria-label="">
        <button
          className={cx(
            'thumb btn',
            buttonClassName ? buttonClassName : defaultButtonClass
          )}
          disabled={current === 1}
          onClick={this.gotoPage.bind(this, current - 1)}
        >
          <FontAwesomeIcon icon={faAngleLeft} size={'sm'} />
        </button>
        {list}
        <button
          className={cx(
            'thumb btn ',
            buttonClassName ? buttonClassName : defaultButtonClass
          )}
          disabled={current === pages}
          onClick={this.gotoPage.bind(this, current + 1)}
        >
          <FontAwesomeIcon icon={faAngleRight} size={'sm'} />
        </button>
      </div>
    );
  }
}

export default PageNav;
