import PropTypes from 'prop-types';
import React, { Component } from 'react';
import createFragment from 'react-addons-create-fragment';
import cx from 'classnames';

class Card extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {}
    };
  }

  render() {
    const {
      children,
      header,
      footer,
      className,
      headerClassName,
      bodyClassName,
      footerClassName
    } = this.props;
    let fragment = {
      header: (
        <div
          className={cx('card-header', headerClassName ? headerClassName : '')}
        >
          {header}
        </div>
      ),
      children: (
        <div className={cx('card-body', bodyClassName ? bodyClassName : '')}>
          {children}
        </div>
      )
    };
    if (footer)
      fragment[footer] = (
        <div
          className={cx('card-footer', footerClassName ? footerClassName : '')}
        >
          {footer}
        </div>
      );
    return (
      <div className={cx('card mb-3', className ? className : '')}>
        {createFragment(fragment)}
      </div>
    );
  }
}

export default Card;

Card.propTypes = {
  children: PropTypes.any.isRequired,
  footer: PropTypes.any,
  header: PropTypes.any.isRequired
};
