import React from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';

import 'react-datepicker/dist/react-datepicker.css';


class formDateRange extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            isEndOpen: false,
            startDate: moment(),
            endDate: moment(),
        };
    }

    handleChange(date) {
        const {onChange} = this.props
        const {startDate, endDate} = this.state
        this.setState({startDate: date})
        this.toggleCalendar()
        onChange({startDate, endDate})
    }

    handleEndDateChange(date) {
        const {onChange} = this.props
        const {startDate, endDate} = this.state
        this.setState({endDate: date})
        this.toggleEndCalendar()
        onChange({startDate, endDate})
    }

    toggleCalendar(e) {
        e && e.preventDefault()
        this.setState({isOpen: !this.state.isOpen})
    }
    toggleEndCalendar(e) {
        e && e.preventDefault()
        this.setState({isEndOpen: !this.state.isEndOpen})
    }

    render() {
        const {id, label} = this.props
        return <div id={id} className="form-group">
            <label htmlFor={id}>{label}</label>
            <div className='form-inline'>
                <div
                    className="form-control "
                    onClick={this.toggleCalendar.bind(this)}>
                    {this.state.startDate.format("DD-MM-YYYY")}
                </div>
                -
                <div
                    className="form-control"
                    onClick={this.toggleEndCalendar.bind(this)}>
                    {this.state.endDate.format("DD-MM-YYYY")}
                </div>
            </div>
            {
                this.state.isOpen && (
                    <DatePicker
                        selected={this.state.startDate}
                        onChange={(date) => this.handleChange(date)}
                        withPortal
                        inline/>
                )
            }
            {
                this.state.isEndOpen && (
                    <DatePicker
                        selected={this.state.endDate}
                        onChange={(date) => this.handleEndDateChange(date)}
                        withPortal
                        inline/>
                )
            }
        </div>
    }
}

export default formDateRange