import React, {Component} from 'react';
import _ from 'lodash';
import cx from 'classnames';

class formSelect extends Component {
    render() {
        const {label, onChange, list, id, value} = this.props
        return (
            <div className="form-group">
                <label htmlFor={id}>{label}</label>
                <select id={id}
                        className={cx("form-control")}
                        onChange={(e) => {
                            onChange(e.target.value)
                        }}
                        value={value === undefined ? '' : value}
                >
                    <option value={''}/>
                    {_.map(list, ({text, value}, index) => {
                        return (
                            <option key={index} value={value}>{text}</option>
                        )
                    })}
                </select>
            </div>
        )
    }
}

export default formSelect;