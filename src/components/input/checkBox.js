import React from 'react';

class CheckBox extends React.Component {
  render() {
    const { id, label, value, onChange } = this.props;
    return (
      <div className="custom-control custom-checkbox">
        <input
          type="checkbox"
          className="custom-control-input"
          id={id}
          checked={value}
          onChange={({ target: { checked } }) => {
            console.log(checked);
            onChange(checked);
          }}
        />
        <label className="custom-control-label" htmlFor={id}>
          {label}
        </label>
      </div>
    );
  }
}

export default CheckBox;
