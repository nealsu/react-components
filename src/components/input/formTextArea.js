import React from 'react';

class FormTextArea extends React.Component {
    render() {
        const {id, label, value, onChange, placeholder} = this.props
        return <div id={id} className="form-group">
            <label htmlFor={id}>{label}</label>
            <textarea id={id}
                   className="form-control"
                   placeholder={placeholder}
                   value={value}
                   onChange={(e) => {
                       onChange(e.target.value)
                   }}
            />
        </div>
    }
}

export default FormTextArea;