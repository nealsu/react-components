import React from 'react';

class Switch extends React.Component {
  render() {
    const { id, label, value, onChange, disabled } = this.props;
    return (
      <div id={id} className="">
        <label className="switch">
          <input
            type="checkbox"
            checked={value}
            onChange={({ target: { checked } }) => {
              onChange(checked);
            }}
            disabled={disabled ? 'disabled' : ''}
          />
          <span className="slider round" />
        </label>
      </div>
    );
  }
}

export default Switch;
