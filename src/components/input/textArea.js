import React from 'react';

class TextArea extends React.Component {
    render() {
        const {id, value, onChange, placeholder} = this.props
        return <textarea id={id}
                         className="form-control"
                         placeholder={placeholder}
                         value={value}
                         onChange={(e) => {
                             onChange(e.target.value)
                         }}
        />
    }
}

export default TextArea;