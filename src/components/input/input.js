import React, { Component } from 'react';
import PropTypes from 'prop-types';

class Input extends Component {
  constructor(props) {
    super(props);
    const { value } = this.props;
    this.state = { newValue: value };
  }

  componentDidMount() {}

  componentWillReceiveProps(nextProps) {
    this.setState({ newValue: nextProps.value });
  }

  componentDidUpdate() {}

  onBlur() {
    this.saveInput();
  }

  onChange(e) {
    const newValue = e.target.value
    this.setState({newValue});
  }

  saveInput() {
    const { onChange, value,forceNumber } = this.props;
    let { newValue } = this.state;
    newValue = !forceNumber? newValue: parseInt(newValue)
    if (value !== newValue) {
      onChange(newValue)
      this.setState({newValue});
    }
  }

  render() {
    const { id, placeholder, disabled} = this.props;
    const { newValue } = this.state;
    return (
      <input
        id={id}
        className={'form-control'}
        placeholder={placeholder ? placeholder : ''}
        style={{ width: '100%' }}
        type="text"
        onBlur={() => this.onBlur()}
        onKeyDown={e => {
          if (e.key === 'Enter') {
            this.onBlur();
          }
        }}
        onChange={e => this.onChange(e)}
        value={newValue ? newValue : ''}
        disabled={disabled ? 'disabled' : ''}
        name="input"
      />
    );
  }
}

export default Input;

Input.propTypes = {
  className: PropTypes.string,
  disabled: PropTypes.bool,
  forceNumber: PropTypes.bool,
  id: PropTypes.string,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  value: PropTypes.any
};
