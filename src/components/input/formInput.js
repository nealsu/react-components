import React from 'react';

class FormInput extends React.Component {
    render() {
        const {id, label, value, onChange, placeholder, disabled} = this.props
        return <div className="form-group">
            <label htmlFor={id}>{label}</label>
            <input id={id}
                   className="form-control"
                   placeholder={placeholder}
                   value={value}
                   disabled={disabled ? "disabled" : ''}
                   onChange={(e) => {
                       onChange(e.target.value)
                   }}
            />
        </div>
    }
}

export default FormInput;