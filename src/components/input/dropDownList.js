import React, { Component } from 'react';
import _ from 'lodash';
import cx from 'classnames';
import PropTypes from 'prop-types';

class DropDownList extends Component {
  render() {
    const { onChange, list, id, value, disabled } = this.props;
    return (
      <select
        key={id}
        className={cx('form-control')}
        onChange={e => {
          onChange(e.target.value);
        }}
        value={_.isNil(value) ? '' : value}
        disabled={disabled ? 'disabled' : ''}
      >
        <option value={''} />
        {_.map(list, ({ value, text }, index) => {
          return (
            <option key={index} value={value}>
              {text}
            </option>
          );
        })}
      </select>
    );
  }
}

export default DropDownList;

DropDownList.propTypes = {
  onChange: PropTypes.func.isRequired,
  id: PropTypes.any,
  list: PropTypes.array.isRequired,
  rules: PropTypes.any,
  validation: PropTypes.any,
  value: PropTypes.any
};
