import React from 'react';
import moment from 'moment';
// import RDatePicker from 'react-datepicker';
import Datetime from 'react-datetime';

import 'react-datetime/css/react-datetime.css';

class DatePicker extends React.Component {
  render() {
    const {
      id,
      onChange,
      disabled = false,
      value,
      showTimeSelect = true
    } = this.props;
    return (
      <div>
        <Datetime
          id={id}
          value={value === '' || !value ? moment() : moment(value)}
          onChange={onChange}
          // showTimeSelect={showTimeSelect}
          // timeIntervals={15}
          dateFormat="YYYY/MM/DD"
          timeFormat={showTimeSelect ? 'HH:mm' : false}
          disabled={disabled}
        />
      </div>

      // <RDatePicker
      //   id={id}
      //   selected={value === '' || !value ? moment() : value}
      //   onChange={onChange}
      //   showTimeSelect
      //   timeFormat="HH:mm"
      //   timeIntervals={15}
      //   dateFormat="YYYY/MM/DD HH:mm"
      //   timeCaption="time"
      //   disabled={disabled}
      // />
    );
  }
}

export default DatePicker;
