import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Tooltip from 'rc-tooltip';
import RcSlider from 'rc-slider';

const Handle = RcSlider.Handle;

class Slider extends Component {
  constructor(props) {
    super(props);
    const { value } = this.props;
    this.state = { newValue: value };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ newValue: nextProps.value });
  }

  onChange(value) {
    const { onChange } = this.props;
    onChange(value);
  }

  saveInput() {
    const { onChange, value, forceNumber } = this.props;
    let { newValue } = this.state;
    newValue = !forceNumber ? newValue : parseInt(newValue);
    if (value !== newValue) {
      onChange(newValue);
      this.setState({ newValue });
    }
  }

  handle(props) {
    const { value, dragging, index, ...restProps } = props;
    return (
      <Tooltip
        prefixCls="rc-slider-tooltip"
        overlay={value}
        visible={dragging}
        placement="top"
        key={index}
      >
        <Handle value={value} {...restProps} />
      </Tooltip>
    );
  }

  render() {
    const { id, disabled, min, max, defaultValue, step } = this.props;
    const { newValue } = this.state;
    return (
      <div>
        <RcSlider
          disabled={disabled}
          min={min}
          max={max}
          step={step}
          defaultValue={defaultValue}
          onChange={value => {
            this.onChange(value);
          }}
          handle={this.handle}
        />
      </div>
    );
  }
}

export default Slider;

Slider.propTypes = {
  className: PropTypes.string,
  disabled: PropTypes.bool,
  id: PropTypes.string,
  onChange: PropTypes.func,
  value: PropTypes.any
};
