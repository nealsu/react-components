import React from 'react'
import cx from 'classnames'
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import {faTimesCircle} from "@fortawesome/fontawesome-free-solid/index";

class FileInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            file: null
        }
    }

    handleChange() {
        let {onChange} = this.props

        let fileInput = this.fileInput
        let fileSub = this.fileSub
        if (fileInput.files.length > 0) {
            let file = fileInput.files[0]
            fileSub.value = file.name
            onChange(file)
            this.setState({
                file
            })
        }
    }

    handleClick() {
        let {onChange} = this.props

        this.fileInput.value = ''
        this.fileSub.value = ''
        onChange(null)
        this.setState({
            file: null
        })
    }

    render() {
        let {id, name, className, placeholder, disabled, enableClear, required, validate, buttonClassName} = this.props
        let {file} = this.state
        let hasFile = !!file
        let extension = (validate && validate.extension) ? validate.extension : ''

        return <div id={id} className={cx('c-file-input', {disabled}, className)}>
            <input
                type='file'
                name={name}
                ref={ref => {
                    this.fileInput = ref
                }}
                accept={extension}
                onChange={() => {
                    this.handleChange()
                }}
                disabled={disabled}
                required={required}/>
            <button className={cx("btn", buttonClassName? buttonClassName : "")} disabled={disabled}>上傳檔案</button>
            <input
                type='text'
                ref={ref => {
                    this.fileSub = ref
                }}
                placeholder={placeholder}
                disabled={disabled}
                readOnly/>
            {enableClear && hasFile && <FontAwesomeIcon icon={faTimesCircle} size={'sm'} onClick={() => {
                this.handleClick()
            }}/>}
        </div>
    }
}

export default FileInput

FileInput.defaultProps = {
    disabled: false,
    enableClear: true,
    required: false,
    validate: {}
}