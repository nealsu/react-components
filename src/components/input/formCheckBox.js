import React from 'react';

class FormCheckBox extends React.Component {
    render() {
        const {id, label, value, onChange} = this.props
        return <div className="form-check">
            <input id={id} className="form-check-input" type="checkbox" checked={value} onChange={({target: {checked}}) => {
                onChange(checked)
            }}/>
            <label className="form-check-label" htmlFor={id}>
                {label}
            </label>
        </div>
    }
}

export default FormCheckBox;