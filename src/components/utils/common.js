/**
 * Created by kevinsu on 2017/3/23.
 */
import _ from 'lodash'

export function renameObjectKey(object, oldkey, newkey) {
    if (object[newkey] !== undefined) {
        alert(`duplicate =>(${newkey})`)
        return object//key重複 不用變
    }
    const clone = _.cloneDeep(object);
    const keyVal = clone[oldkey];

    delete clone[oldkey];
    clone[newkey] = keyVal;
    return clone;
}

export function handleObjectAdd(object, key, objectToAdd) {
    if (object[key] !== undefined) {
        alert(`duplicate key(${key})`)
        return object
    }
    switch (typeof objectToAdd) {
        case 'string':
            object[key] = objectToAdd
            break
        case 'object':
            object[key] = copyObject(objectToAdd)
            break
        default:
            break
    }
    return object
}

export function handleObjectDelete(object, key) {
    delete object[key]
    return object
}

export function handleArrayAdd(array, item) {
    array = _.cloneDeep(array)
    if (array === undefined)
        array = [];
    array.push(item)
    return array
}

export function handleArrayDelete(array, index) {
    array.splice(index, 1);
    return array
}

export function sx() {
    let newStyle = {}
    for (let i = 0; i < arguments.length; i++) {
        _.map(arguments[i], (style, key) => {
            newStyle[key] = style
        })
    }
    // console.log(newStyle)
    return newStyle
}

export function cx() {
    let newClass = ''
    for (let i = 0; i < arguments.length; i++) {
        newClass += arguments[i] + ' '
    }
    return newClass
}

export function nestedObjectParse(dataType_properties, rootName = '') {
    var resultProperties = []
    _.map(dataType_properties, (property, key) => {
        if (property.type !== undefined && typeof property.type === 'string') {
            if (property.type === 'image' && key === "properties")
                resultProperties.push(rootName)
            else
                resultProperties.push(rootName ? (rootName + "." + key) : key)
        }
        else {
            if (key === 'fields') {
                //do nothing
            }
            else if (key !== 'properties')
                resultProperties = resultProperties.concat(this.nestedObjectParse(property, rootName ? (rootName + "." + key) : key))
            else
                resultProperties = resultProperties.concat(this.nestedObjectParse(property, rootName))
        }
    })
    return resultProperties
}

export function nestedObjectParseWithObjectKey(dataType_properties, rootName = '') {
    var resultProperties = []
    _.map(dataType_properties, (property, key) => {
        if (property.type !== undefined && typeof property.type === 'string') {
            if (property.type === 'image' && key === "properties")
                resultProperties.push(rootName)
            else
                resultProperties.push(rootName ? (rootName + "." + key) : key)
        }
        else {
            resultProperties.push(rootName ? (rootName + "." + key) : key)
            resultProperties = resultProperties.concat(this.nestedObjectParse(property.properties, rootName ? (rootName + "." + key) : key))
        }
    })
    return resultProperties
}

export function copyObject(object) {
    return JSON.parse(JSON.stringify(object))
}

export function sortByObjectValue(object, orderValue) {

    return _.chain(object)
        .map((val, key) => {
            return {__theObjectKey: key, ...val}
        })
        .sortBy(orderValue)
        .keyBy('__theObjectKey')
        .mapValues((o) => {
            delete o.__theObjectKey
            return o
        })
        .value();
}

export function sortByObjectKey(object, orderValue) {

    return sortByObjectValue(object, '__theObjectKey')
}

// export function GetValueInReactComponent(reactComponent, rootObjName, nestInfo) {
//     const rootObject = reactComponent.state[rootObjName];
//     return TraceNestedValue(rootObject, nestInfo)
// }

// function TraceNestedValue(rootObject, nestInfo) {
//     let nowObject;
//     var targetKey = nestInfo[nestInfo.length - 1][1]//最後一個key
//     nowObject = TraceNestedObject(rootObject, nestInfo)
//     var result = nowObject[targetKey];
//     // console.log(result)
//     if (!result)
//         return '';
//     else
//         return result;
// }
// function TraceNestedObject(rootObject, nestInfo) {
//     let nowObject = rootObject;
//     let nowNestInfo, index, key;
//     while (nestInfo.length !== 0) {
//         nowNestInfo = nestInfo.shift();
//         index = nowNestInfo[0];
//         key = nowNestInfo[1];
//         if (nestInfo.length === 0)
//             nowObject = nowObject[index];//最後一筆
//         else
//             nowObject = nowObject[index][key];
//     }
//     return nowObject;
// }

// $(document).ready(function () {
//     // $('a').click(function (e) {
//     //     e.preventDefault();
//     // });
//     }
// )
