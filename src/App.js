import React, { Component } from 'react';
import { push } from 'react-router-redux';
import { Route, NavLink, withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { setEnv } from './modules/env';
import logo from './logo.svg';
import './components/dist/App.css';
import FormExample from './containers/formExample';
import TableExample from './containers/tableExample';
import NavExample from './containers/navExample';
import LoaderSpinnerExample from './containers/loaderSpinnerExample';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="App">
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarTogglerDemo01"
            aria-controls="navbarTogglerDemo01"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarTogglerDemo01">
            <a className="navbar-brand" href="#">
              Hidden brand
            </a>
            <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
              <li className="nav-item active">
                <NavLink className="nav-link" to="/TableExample">
                  Table
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/formExample">
                  Form
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/navExample">
                  Nav
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/loaderSpinnerExample">
                  LoaderSpinnerExample
                </NavLink>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Disabled
                </a>
              </li>
            </ul>
          </div>
        </nav>
        <div>
          <main>
            <Route exact path="/TableExample" component={TableExample} />
            <Route path="/formExample" component={FormExample} />
            <Route path="/navExample" component={NavExample} />
            <Route
              path="/loaderSpinnerExample"
              component={LoaderSpinnerExample}
            />
          </main>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  env: state.env
});
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setEnv,
      changePage: () => push('/about-us')
    },
    dispatch
  );

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
// export default App;
