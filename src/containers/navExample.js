import React, {Component} from 'react';
import {Nav} from 'src/components'
import * as Common from 'src/components/utils/common'

class NavExample extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rawData: [],
            navIndex:'navIndex1'
        }
    }

    handleAdd() {
        let {rawData} = this.state
        rawData = Common.handleArrayAdd(rawData, {})
        this.setState({rawData})
    }

    render() {
        const {navIndex} = this.state
        const menu={
            navIndex1:{label:'tab1',content:<div>tab1 content</div>},
            navIndex2:{label:'tab2',content:<div>tab2 content</div>}
        }
        return (
            <div className='container'>
                <Nav
                    id='locales'
                    menu={menu}
                    menuIndex={navIndex}
                    onChange={(navIndex) => this.setState({navIndex})}
                />
            </div>
        );
    }
}

export default NavExample
// export default App;
