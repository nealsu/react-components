import React, {Component} from 'react';
import {DataTable, Card, Input, PageNav} from 'src/components'
import * as Common from 'src/components/utils/common'

class FormExample extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rawData: []
        }
    }

    handleAdd() {
        let {rawData} = this.state
        rawData = Common.handleArrayAdd(rawData, {})
        this.setState({rawData})
    }

    render() {
        const {rawData, nowPage=1} = this.state
        const pageSlice = 5
        const pages = rawData.length % pageSlice !== 0 ? Math.floor(rawData.length / pageSlice) + 1 : Math.floor(rawData.length / pageSlice)
        let nowData = rawData.slice((nowPage - 1) * pageSlice, ((nowPage) * pageSlice))
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col-6'>
                        <DataTable
                            id="Page_fields"
                            rawData={nowData}
                            emptyRow={{}}
                            fields={{
                                validRules: {
                                    title: 'Valid　Rules',
                                    sortable: true
                                },
                                order: {
                                    title: 'order'
                                },
                            }}
                            onSort={(key, sort) => {
                                console.log(key, sort)
                            }}
                        />
                        <PageNav
                            pages={pages}
                            current={nowPage}
                            onChange={nowPage => this.setState({nowPage})}
                        />
                    </div>
                    <div className='col-6'>
                        <Card
                            header={'輸出結果'}
                        >
                            {JSON.stringify(rawData, null, '\t')}
                        </Card>
                    </div>
                </div>
                <div className='row'>
                    <div className='col-6'>
                        <DataTable
                            id="Editor_fields"
                            addable={true}
                            deletable={true}
                            rawData={rawData}
                            emptyRow={{}}
                            fields={{
                                validRules: {
                                    title: 'Valid　Rules',
                                    editor: Input.Input,
                                    sortable:true
                                },
                                order: {
                                    title: 'order',
                                    editor: Input.Input
                                },
                            }}
                            handleChange={(rawData) => {
                                this.setState({rawData})
                            }}
                            onSort={(key,sort) => {
                                console.log(key,sort)
                            }}
                        />
                    </div>
                    <div className='col-6'>
                        <Card
                            header={'輸出結果'}
                        >
                            {JSON.stringify(rawData, null, '\t')}
                        </Card>
                    </div>
                </div>
            </div>
        );
    }
}

export default FormExample
// export default App;
