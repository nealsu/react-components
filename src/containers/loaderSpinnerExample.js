import React, { Component } from 'react';
import { LoaderSpinner } from 'src/components';

class loaderSpinnerExample extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div>
        <button
          style={{ margin: '20px' }}
          onClick={() => {
            LoaderSpinner.start();
            setTimeout(() => {
              LoaderSpinner.stop();
            }, 1000);
          }}
        >
          Test
        </button>
        {/*<LoaderSpinner*/}
        {/*ref={(ref) => {*/}
        {/*this.loaderSpinner = ref*/}
        {/*}}*/}
        {/*/>*/}
      </div>
    );
  }
}

export default loaderSpinnerExample;
// export default App;
