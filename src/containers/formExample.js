import React, { Component } from 'react';
import { Form, Card, Input } from 'src/components';
class FormExample extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {}
    };
  }

  render() {
    const { formData } = this.state;
    return (
      <div className="container">
        <div className="row">
          <div className="col-6">
            <Card header={'測試'}>
              <Form
                fields={{
                  INPUT: {
                    title: '文字',
                    editor: Input.Input,
                    props: { placeholder: 'placeholder test' }
                  },
                  CHECK_BOX: {
                    title: '卻科霸可思',
                    editor: Input.CheckBox,
                    props: { label: '是' }
                  },
                  TEXT_AREA: { title: '文字區塊', editor: Input.TextArea },
                  SWITCH: { title: '開關', editor: Input.Switch },
                  DROP_DOWN_LIST: {
                    title: '下拉式選單',
                    editor: Input.DropDownList,
                    props: {
                      list: [
                        { value: 'value1', text: 'text1' },
                        { value: 'value2', text: 'text2' },
                        { value: 'value3', text: 'text3' }
                      ]
                    }
                  },
                  DATE_PICKER: {
                    title: 'DATE_PICKER',
                    editor: Input.DatePicker
                  },
                  FILE_INPUT: {
                    title: 'FILE_INPUT',
                    editor: Input.FileInput,
                    props: {
                      buttonClassName: 'buttonClassNameTest'
                    }
                  },
                  SLIDER: {
                    title: 'SLIDER',
                    editor: Input.Slider
                  }
                }}
                data={formData}
                onChange={formData => {
                  this.setState({ formData });
                }}
              />
            </Card>
          </div>
          <div className="col-6">
            <Card header={'輸出結果'}>
              {JSON.stringify(formData, null, '\t')}
            </Card>
          </div>
        </div>
      </div>
    );
  }
}

export default FormExample;
// export default App;
