'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DropDownList = function (_Component) {
  _inherits(DropDownList, _Component);

  function DropDownList() {
    _classCallCheck(this, DropDownList);

    return _possibleConstructorReturn(this, (DropDownList.__proto__ || Object.getPrototypeOf(DropDownList)).apply(this, arguments));
  }

  _createClass(DropDownList, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          _onChange = _props.onChange,
          list = _props.list,
          id = _props.id,
          value = _props.value,
          disabled = _props.disabled;

      return _react2.default.createElement(
        'select',
        {
          key: id,
          className: (0, _classnames2.default)('form-control'),
          onChange: function onChange(e) {
            _onChange(e.target.value);
          },
          value: _lodash2.default.isNil(value) ? '' : value,
          disabled: disabled ? 'disabled' : ''
        },
        _react2.default.createElement('option', { value: '' }),
        _lodash2.default.map(list, function (_ref, index) {
          var value = _ref.value,
              text = _ref.text;

          return _react2.default.createElement(
            'option',
            { key: index, value: value },
            text
          );
        })
      );
    }
  }]);

  return DropDownList;
}(_react.Component);

exports.default = DropDownList;


DropDownList.propTypes = {
  onChange: _propTypes2.default.func.isRequired,
  id: _propTypes2.default.any,
  list: _propTypes2.default.array.isRequired,
  rules: _propTypes2.default.any,
  validation: _propTypes2.default.any,
  value: _propTypes2.default.any
};