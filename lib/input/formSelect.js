'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var formSelect = function (_Component) {
    _inherits(formSelect, _Component);

    function formSelect() {
        _classCallCheck(this, formSelect);

        return _possibleConstructorReturn(this, (formSelect.__proto__ || Object.getPrototypeOf(formSelect)).apply(this, arguments));
    }

    _createClass(formSelect, [{
        key: 'render',
        value: function render() {
            var _props = this.props,
                label = _props.label,
                _onChange = _props.onChange,
                list = _props.list,
                id = _props.id,
                value = _props.value;

            return _react2.default.createElement(
                'div',
                { className: 'form-group' },
                _react2.default.createElement(
                    'label',
                    { htmlFor: id },
                    label
                ),
                _react2.default.createElement(
                    'select',
                    { id: id,
                        className: (0, _classnames2.default)("form-control"),
                        onChange: function onChange(e) {
                            _onChange(e.target.value);
                        },
                        value: value === undefined ? '' : value
                    },
                    _react2.default.createElement('option', { value: '' }),
                    _lodash2.default.map(list, function (_ref, index) {
                        var text = _ref.text,
                            value = _ref.value;

                        return _react2.default.createElement(
                            'option',
                            { key: index, value: value },
                            text
                        );
                    })
                )
            );
        }
    }]);

    return formSelect;
}(_react.Component);

exports.default = formSelect;