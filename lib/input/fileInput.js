'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _reactFontawesome = require('@fortawesome/react-fontawesome');

var _reactFontawesome2 = _interopRequireDefault(_reactFontawesome);

var _index = require('@fortawesome/fontawesome-free-solid/index');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var FileInput = function (_React$Component) {
    _inherits(FileInput, _React$Component);

    function FileInput(props) {
        _classCallCheck(this, FileInput);

        var _this = _possibleConstructorReturn(this, (FileInput.__proto__ || Object.getPrototypeOf(FileInput)).call(this, props));

        _this.state = {
            file: null
        };
        return _this;
    }

    _createClass(FileInput, [{
        key: 'handleChange',
        value: function handleChange() {
            var onChange = this.props.onChange;


            var fileInput = this.fileInput;
            var fileSub = this.fileSub;
            if (fileInput.files.length > 0) {
                var file = fileInput.files[0];
                fileSub.value = file.name;
                onChange(file);
                this.setState({
                    file: file
                });
            }
        }
    }, {
        key: 'handleClick',
        value: function handleClick() {
            var onChange = this.props.onChange;


            this.fileInput.value = '';
            this.fileSub.value = '';
            onChange(null);
            this.setState({
                file: null
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var _props = this.props,
                id = _props.id,
                name = _props.name,
                className = _props.className,
                placeholder = _props.placeholder,
                disabled = _props.disabled,
                enableClear = _props.enableClear,
                required = _props.required,
                validate = _props.validate,
                buttonClassName = _props.buttonClassName;
            var file = this.state.file;

            var hasFile = !!file;
            var extension = validate && validate.extension ? validate.extension : '';

            return _react2.default.createElement(
                'div',
                { id: id, className: (0, _classnames2.default)('c-file-input', { disabled: disabled }, className) },
                _react2.default.createElement('input', {
                    type: 'file',
                    name: name,
                    ref: function ref(_ref) {
                        _this2.fileInput = _ref;
                    },
                    accept: extension,
                    onChange: function onChange() {
                        _this2.handleChange();
                    },
                    disabled: disabled,
                    required: required }),
                _react2.default.createElement(
                    'button',
                    { className: (0, _classnames2.default)("btn", buttonClassName ? buttonClassName : ""), disabled: disabled },
                    '\u4E0A\u50B3\u6A94\u6848'
                ),
                _react2.default.createElement('input', {
                    type: 'text',
                    ref: function ref(_ref2) {
                        _this2.fileSub = _ref2;
                    },
                    placeholder: placeholder,
                    disabled: disabled,
                    readOnly: true }),
                enableClear && hasFile && _react2.default.createElement(_reactFontawesome2.default, { icon: _index.faTimesCircle, size: 'sm', onClick: function onClick() {
                        _this2.handleClick();
                    } })
            );
        }
    }]);

    return FileInput;
}(_react2.default.Component);

exports.default = FileInput;


FileInput.defaultProps = {
    disabled: false,
    enableClear: true,
    required: false,
    validate: {}
};