'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Input = function (_Component) {
  _inherits(Input, _Component);

  function Input(props) {
    _classCallCheck(this, Input);

    var _this = _possibleConstructorReturn(this, (Input.__proto__ || Object.getPrototypeOf(Input)).call(this, props));

    var value = _this.props.value;

    _this.state = { newValue: value };
    return _this;
  }

  _createClass(Input, [{
    key: 'componentDidMount',
    value: function componentDidMount() {}
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(nextProps) {
      this.setState({ newValue: nextProps.value });
    }
  }, {
    key: 'componentDidUpdate',
    value: function componentDidUpdate() {}
  }, {
    key: 'onBlur',
    value: function onBlur() {
      this.saveInput();
    }
  }, {
    key: 'onChange',
    value: function onChange(e) {
      var newValue = e.target.value;
      this.setState({ newValue: newValue });
    }
  }, {
    key: 'saveInput',
    value: function saveInput() {
      var _props = this.props,
          onChange = _props.onChange,
          value = _props.value,
          forceNumber = _props.forceNumber;
      var newValue = this.state.newValue;

      newValue = !forceNumber ? newValue : parseInt(newValue);
      if (value !== newValue) {
        onChange(newValue);
        this.setState({ newValue: newValue });
      }
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props2 = this.props,
          id = _props2.id,
          placeholder = _props2.placeholder,
          disabled = _props2.disabled;
      var newValue = this.state.newValue;

      return _react2.default.createElement('input', {
        id: id,
        className: 'form-control',
        placeholder: placeholder ? placeholder : '',
        style: { width: '100%' },
        type: 'text',
        onBlur: function onBlur() {
          return _this2.onBlur();
        },
        onKeyDown: function onKeyDown(e) {
          if (e.key === 'Enter') {
            _this2.onBlur();
          }
        },
        onChange: function onChange(e) {
          return _this2.onChange(e);
        },
        value: newValue ? newValue : '',
        disabled: disabled ? 'disabled' : '',
        name: 'input'
      });
    }
  }]);

  return Input;
}(_react.Component);

exports.default = Input;


Input.propTypes = {
  className: _propTypes2.default.string,
  disabled: _propTypes2.default.bool,
  forceNumber: _propTypes2.default.bool,
  id: _propTypes2.default.string,
  onChange: _propTypes2.default.func,
  placeholder: _propTypes2.default.string,
  value: _propTypes2.default.any
};