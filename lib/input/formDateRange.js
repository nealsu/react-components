'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDatepicker = require('react-datepicker');

var _reactDatepicker2 = _interopRequireDefault(_reactDatepicker);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

require('react-datepicker/dist/react-datepicker.css');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var formDateRange = function (_React$Component) {
    _inherits(formDateRange, _React$Component);

    function formDateRange(props) {
        _classCallCheck(this, formDateRange);

        var _this = _possibleConstructorReturn(this, (formDateRange.__proto__ || Object.getPrototypeOf(formDateRange)).call(this, props));

        _this.state = {
            isOpen: false,
            isEndOpen: false,
            startDate: (0, _moment2.default)(),
            endDate: (0, _moment2.default)()
        };
        return _this;
    }

    _createClass(formDateRange, [{
        key: 'handleChange',
        value: function handleChange(date) {
            var onChange = this.props.onChange;
            var _state = this.state,
                startDate = _state.startDate,
                endDate = _state.endDate;

            this.setState({ startDate: date });
            this.toggleCalendar();
            onChange({ startDate: startDate, endDate: endDate });
        }
    }, {
        key: 'handleEndDateChange',
        value: function handleEndDateChange(date) {
            var onChange = this.props.onChange;
            var _state2 = this.state,
                startDate = _state2.startDate,
                endDate = _state2.endDate;

            this.setState({ endDate: date });
            this.toggleEndCalendar();
            onChange({ startDate: startDate, endDate: endDate });
        }
    }, {
        key: 'toggleCalendar',
        value: function toggleCalendar(e) {
            e && e.preventDefault();
            this.setState({ isOpen: !this.state.isOpen });
        }
    }, {
        key: 'toggleEndCalendar',
        value: function toggleEndCalendar(e) {
            e && e.preventDefault();
            this.setState({ isEndOpen: !this.state.isEndOpen });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var _props = this.props,
                id = _props.id,
                label = _props.label;

            return _react2.default.createElement(
                'div',
                { id: id, className: 'form-group' },
                _react2.default.createElement(
                    'label',
                    { htmlFor: id },
                    label
                ),
                _react2.default.createElement(
                    'div',
                    { className: 'form-inline' },
                    _react2.default.createElement(
                        'div',
                        {
                            className: 'form-control ',
                            onClick: this.toggleCalendar.bind(this) },
                        this.state.startDate.format("DD-MM-YYYY")
                    ),
                    '-',
                    _react2.default.createElement(
                        'div',
                        {
                            className: 'form-control',
                            onClick: this.toggleEndCalendar.bind(this) },
                        this.state.endDate.format("DD-MM-YYYY")
                    )
                ),
                this.state.isOpen && _react2.default.createElement(_reactDatepicker2.default, {
                    selected: this.state.startDate,
                    onChange: function onChange(date) {
                        return _this2.handleChange(date);
                    },
                    withPortal: true,
                    inline: true }),
                this.state.isEndOpen && _react2.default.createElement(_reactDatepicker2.default, {
                    selected: this.state.endDate,
                    onChange: function onChange(date) {
                        return _this2.handleEndDateChange(date);
                    },
                    withPortal: true,
                    inline: true })
            );
        }
    }]);

    return formDateRange;
}(_react2.default.Component);

exports.default = formDateRange;