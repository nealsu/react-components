'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _rcTooltip = require('rc-tooltip');

var _rcTooltip2 = _interopRequireDefault(_rcTooltip);

var _rcSlider = require('rc-slider');

var _rcSlider2 = _interopRequireDefault(_rcSlider);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Handle = _rcSlider2.default.Handle;

var Slider = function (_Component) {
    _inherits(Slider, _Component);

    function Slider(props) {
        _classCallCheck(this, Slider);

        var _this = _possibleConstructorReturn(this, (Slider.__proto__ || Object.getPrototypeOf(Slider)).call(this, props));

        var value = _this.props.value;

        _this.state = { newValue: value };
        return _this;
    }

    _createClass(Slider, [{
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            this.setState({ newValue: nextProps.value });
        }
    }, {
        key: 'onChange',
        value: function onChange(value) {
            var onChange = this.props.onChange;

            onChange(value);
        }
    }, {
        key: 'saveInput',
        value: function saveInput() {
            var _props = this.props,
                onChange = _props.onChange,
                value = _props.value,
                forceNumber = _props.forceNumber;
            var newValue = this.state.newValue;

            newValue = !forceNumber ? newValue : parseInt(newValue);
            if (value !== newValue) {
                onChange(newValue);
                this.setState({ newValue: newValue });
            }
        }
    }, {
        key: 'handle',
        value: function handle(props) {
            var value = props.value,
                dragging = props.dragging,
                index = props.index,
                restProps = _objectWithoutProperties(props, ['value', 'dragging', 'index']);

            return _react2.default.createElement(
                _rcTooltip2.default,
                {
                    prefixCls: 'rc-slider-tooltip',
                    overlay: value,
                    visible: dragging,
                    placement: 'top',
                    key: index
                },
                _react2.default.createElement(Handle, _extends({ value: value }, restProps))
            );
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var _props2 = this.props,
                id = _props2.id,
                disabled = _props2.disabled,
                min = _props2.min,
                max = _props2.max,
                defaultValue = _props2.defaultValue,
                step = _props2.step;
            var newValue = this.state.newValue;

            return _react2.default.createElement(
                'div',
                null,
                _react2.default.createElement(_rcSlider2.default, {
                    disabled: disabled,
                    min: min,
                    max: max,
                    step: step,
                    defaultValue: defaultValue,
                    onChange: function onChange(value) {
                        _this2.onChange(value);
                    },
                    handle: this.handle
                })
            );
        }
    }]);

    return Slider;
}(_react.Component);

exports.default = Slider;


Slider.propTypes = {
    className: _propTypes2.default.string,
    disabled: _propTypes2.default.bool,
    id: _propTypes2.default.string,
    onChange: _propTypes2.default.func,
    value: _propTypes2.default.any
};