'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _reactDatetime = require('react-datetime');

var _reactDatetime2 = _interopRequireDefault(_reactDatetime);

require('react-datetime/css/react-datetime.css');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
// import RDatePicker from 'react-datepicker';


var DatePicker = function (_React$Component) {
  _inherits(DatePicker, _React$Component);

  function DatePicker() {
    _classCallCheck(this, DatePicker);

    return _possibleConstructorReturn(this, (DatePicker.__proto__ || Object.getPrototypeOf(DatePicker)).apply(this, arguments));
  }

  _createClass(DatePicker, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          id = _props.id,
          onChange = _props.onChange,
          _props$disabled = _props.disabled,
          disabled = _props$disabled === undefined ? false : _props$disabled,
          value = _props.value,
          _props$showTimeSelect = _props.showTimeSelect,
          showTimeSelect = _props$showTimeSelect === undefined ? true : _props$showTimeSelect;

      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(_reactDatetime2.default, {
          id: id,
          value: value === '' || !value ? (0, _moment2.default)() : (0, _moment2.default)(value),
          onChange: onChange
          // showTimeSelect={showTimeSelect}
          // timeIntervals={15}
          , dateFormat: 'YYYY/MM/DD',
          timeFormat: showTimeSelect ? 'HH:mm' : false,
          disabled: disabled
        })
      )

      // <RDatePicker
      //   id={id}
      //   selected={value === '' || !value ? moment() : value}
      //   onChange={onChange}
      //   showTimeSelect
      //   timeFormat="HH:mm"
      //   timeIntervals={15}
      //   dateFormat="YYYY/MM/DD HH:mm"
      //   timeCaption="time"
      //   disabled={disabled}
      // />
      ;
    }
  }]);

  return DatePicker;
}(_react2.default.Component);

exports.default = DatePicker;