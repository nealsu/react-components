'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _reactFontawesome = require('@fortawesome/react-fontawesome');

var _reactFontawesome2 = _interopRequireDefault(_reactFontawesome);

var _index = require('@fortawesome/fontawesome-free-solid/index');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * @param {number} [pages] - Total number of pages
 * @param {number} [current=1] - Current (highlighted) page number
 * @param {number} [thumbnails=9] - Maximum number of thumbnails to display
 * @param {string} [className] - Classname for the container
 * @param {function} onChange - Callback function when from/to is changed. <br> Required when value prop is supplied
 * @param {number} onChange.page - current selected page
 */
var defaultButtonClass = 'btn-secondary';

var PageNav = function (_React$Component) {
  _inherits(PageNav, _React$Component);

  function PageNav() {
    _classCallCheck(this, PageNav);

    return _possibleConstructorReturn(this, (PageNav.__proto__ || Object.getPrototypeOf(PageNav)).apply(this, arguments));
  }

  _createClass(PageNav, [{
    key: 'gotoPage',

    // propTypes: {
    //     pages: React.PropTypes.number,
    //     current: React.PropTypes.number,
    //     thumbnails: React.PropTypes.number,
    //     className: React.PropTypes.string,
    //     onChange: React.PropTypes.func.isRequired
    // }
    // getDefaultProps() {
    //     return {
    //         pages: null,
    //         current: 1,
    //         thumbnails: 9
    //     }
    // }
    value: function gotoPage(page) {
      var onChange = this.props.onChange;

      onChange(page);
    }
  }, {
    key: 'renderThumb',
    value: function renderThumb(page, key) {
      var _props = this.props,
          current = _props.current,
          buttonClassName = _props.buttonClassName;

      return _react2.default.createElement(
        'button',
        {
          key: key,
          className: (0, _classnames2.default)('thumb btn', { active: current === page }, buttonClassName ? buttonClassName : defaultButtonClass),
          disabled: !page,
          onClick: this.gotoPage.bind(this, page)
        },
        page || '...'
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _props2 = this.props,
          _props2$thumbnails = _props2.thumbnails,
          thumbnails = _props2$thumbnails === undefined ? 9 : _props2$thumbnails,
          current = _props2.current,
          pages = _props2.pages,
          className = _props2.className,
          buttonClassName = _props2.buttonClassName;


      if (!pages) {
        return _react2.default.createElement(
          'div',
          { className: (0, _classnames2.default)('btn-group', className), role: 'group', 'aria-label': '' },
          _react2.default.createElement(
            'button',
            {
              className: (0, _classnames2.default)('thumb btn', buttonClassName ? buttonClassName : defaultButtonClass),
              disabled: true
            },
            _react2.default.createElement(_reactFontawesome2.default, { icon: _index.faAngleLeft, size: 'sm' })
          ),
          _react2.default.createElement(
            'button',
            {
              className: (0, _classnames2.default)('thumb btn', buttonClassName ? buttonClassName : defaultButtonClass),
              disabled: true
            },
            '0'
          ),
          _react2.default.createElement(
            'button',
            {
              className: (0, _classnames2.default)('thumb btn ', buttonClassName ? buttonClassName : defaultButtonClass),
              disabled: true
            },
            _react2.default.createElement(_reactFontawesome2.default, { icon: _index.faAngleRight, size: 'sm' })
          )
        );
      }

      // let endThumbs = Math.floor(thumbnails/4);
      var endThumbs = 2; // display 2 at both ends
      var midThumbs = thumbnails - endThumbs * 2 - 2;
      var list = [];

      var midStart = Math.max(current - Math.floor(midThumbs / 2), endThumbs + 1);
      var midEnd = midStart + midThumbs - 1;
      var lastSkipped = false;

      if (midEnd >= pages - endThumbs) {
        midStart = Math.max(endThumbs + 1, midStart - (midEnd - (pages - endThumbs)));
        midEnd = pages - endThumbs;
        midStart--;
      }

      if (midStart === endThumbs + 1) {
        midEnd++;
      }

      if (midStart === endThumbs + 2) {
        midStart--;
      }
      if (midEnd === pages - endThumbs - 1) {
        midEnd++;
      }

      _lodash2.default.forEach(_lodash2.default.range(1, pages + 1), function (i) {
        if (i <= endThumbs || i > pages - endThumbs || i >= midStart && i <= midEnd) {
          list.push(_this2.renderThumb(i, i));
          lastSkipped = false;
        } else {
          if (!lastSkipped) {
            list.push(_this2.renderThumb(null, i));
            lastSkipped = true;
          }
        }
      });

      return _react2.default.createElement(
        'div',
        { className: (0, _classnames2.default)('btn-group', className), role: 'group', 'aria-label': '' },
        _react2.default.createElement(
          'button',
          {
            className: (0, _classnames2.default)('thumb btn', buttonClassName ? buttonClassName : defaultButtonClass),
            disabled: current === 1,
            onClick: this.gotoPage.bind(this, current - 1)
          },
          _react2.default.createElement(_reactFontawesome2.default, { icon: _index.faAngleLeft, size: 'sm' })
        ),
        list,
        _react2.default.createElement(
          'button',
          {
            className: (0, _classnames2.default)('thumb btn ', buttonClassName ? buttonClassName : defaultButtonClass),
            disabled: current === pages,
            onClick: this.gotoPage.bind(this, current + 1)
          },
          _react2.default.createElement(_reactFontawesome2.default, { icon: _index.faAngleRight, size: 'sm' })
        )
      );
    }
  }]);

  return PageNav;
}(_react2.default.Component);

exports.default = PageNav;