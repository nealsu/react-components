'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; /**
                                                                                                                                                                                                                                                                               * Created by kevinsu on 2017/3/23.
                                                                                                                                                                                                                                                                               */


exports.renameObjectKey = renameObjectKey;
exports.handleObjectAdd = handleObjectAdd;
exports.handleObjectDelete = handleObjectDelete;
exports.handleArrayAdd = handleArrayAdd;
exports.handleArrayDelete = handleArrayDelete;
exports.sx = sx;
exports.cx = cx;
exports.nestedObjectParse = nestedObjectParse;
exports.nestedObjectParseWithObjectKey = nestedObjectParseWithObjectKey;
exports.copyObject = copyObject;
exports.sortByObjectValue = sortByObjectValue;
exports.sortByObjectKey = sortByObjectKey;

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function renameObjectKey(object, oldkey, newkey) {
    if (object[newkey] !== undefined) {
        alert('duplicate =>(' + newkey + ')');
        return object; //key重複 不用變
    }
    var clone = _lodash2.default.cloneDeep(object);
    var keyVal = clone[oldkey];

    delete clone[oldkey];
    clone[newkey] = keyVal;
    return clone;
}

function handleObjectAdd(object, key, objectToAdd) {
    if (object[key] !== undefined) {
        alert('duplicate key(' + key + ')');
        return object;
    }
    switch (typeof objectToAdd === 'undefined' ? 'undefined' : _typeof(objectToAdd)) {
        case 'string':
            object[key] = objectToAdd;
            break;
        case 'object':
            object[key] = copyObject(objectToAdd);
            break;
        default:
            break;
    }
    return object;
}

function handleObjectDelete(object, key) {
    delete object[key];
    return object;
}

function handleArrayAdd(array, item) {
    array = _lodash2.default.cloneDeep(array);
    if (array === undefined) array = [];
    array.push(item);
    return array;
}

function handleArrayDelete(array, index) {
    array.splice(index, 1);
    return array;
}

function sx() {
    var newStyle = {};
    for (var i = 0; i < arguments.length; i++) {
        _lodash2.default.map(arguments[i], function (style, key) {
            newStyle[key] = style;
        });
    }
    // console.log(newStyle)
    return newStyle;
}

function cx() {
    var newClass = '';
    for (var i = 0; i < arguments.length; i++) {
        newClass += arguments[i] + ' ';
    }
    return newClass;
}

function nestedObjectParse(dataType_properties) {
    var _this = this;

    var rootName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

    var resultProperties = [];
    _lodash2.default.map(dataType_properties, function (property, key) {
        if (property.type !== undefined && typeof property.type === 'string') {
            if (property.type === 'image' && key === "properties") resultProperties.push(rootName);else resultProperties.push(rootName ? rootName + "." + key : key);
        } else {
            if (key === 'fields') {
                //do nothing
            } else if (key !== 'properties') resultProperties = resultProperties.concat(_this.nestedObjectParse(property, rootName ? rootName + "." + key : key));else resultProperties = resultProperties.concat(_this.nestedObjectParse(property, rootName));
        }
    });
    return resultProperties;
}

function nestedObjectParseWithObjectKey(dataType_properties) {
    var _this2 = this;

    var rootName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

    var resultProperties = [];
    _lodash2.default.map(dataType_properties, function (property, key) {
        if (property.type !== undefined && typeof property.type === 'string') {
            if (property.type === 'image' && key === "properties") resultProperties.push(rootName);else resultProperties.push(rootName ? rootName + "." + key : key);
        } else {
            resultProperties.push(rootName ? rootName + "." + key : key);
            resultProperties = resultProperties.concat(_this2.nestedObjectParse(property.properties, rootName ? rootName + "." + key : key));
        }
    });
    return resultProperties;
}

function copyObject(object) {
    return JSON.parse(JSON.stringify(object));
}

function sortByObjectValue(object, orderValue) {

    return _lodash2.default.chain(object).map(function (val, key) {
        return _extends({ __theObjectKey: key }, val);
    }).sortBy(orderValue).keyBy('__theObjectKey').mapValues(function (o) {
        delete o.__theObjectKey;
        return o;
    }).value();
}

function sortByObjectKey(object, orderValue) {

    return sortByObjectValue(object, '__theObjectKey');
}

// export function GetValueInReactComponent(reactComponent, rootObjName, nestInfo) {
//     const rootObject = reactComponent.state[rootObjName];
//     return TraceNestedValue(rootObject, nestInfo)
// }

// function TraceNestedValue(rootObject, nestInfo) {
//     let nowObject;
//     var targetKey = nestInfo[nestInfo.length - 1][1]//最後一個key
//     nowObject = TraceNestedObject(rootObject, nestInfo)
//     var result = nowObject[targetKey];
//     // console.log(result)
//     if (!result)
//         return '';
//     else
//         return result;
// }
// function TraceNestedObject(rootObject, nestInfo) {
//     let nowObject = rootObject;
//     let nowNestInfo, index, key;
//     while (nestInfo.length !== 0) {
//         nowNestInfo = nestInfo.shift();
//         index = nowNestInfo[0];
//         key = nowNestInfo[1];
//         if (nestInfo.length === 0)
//             nowObject = nowObject[index];//最後一筆
//         else
//             nowObject = nowObject[index][key];
//     }
//     return nowObject;
// }

// $(document).ready(function () {
//     // $('a').click(function (e) {
//     //     e.preventDefault();
//     // });
//     }
// )