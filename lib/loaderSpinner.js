'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

exports.initial = initial;

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactDom = require('react-dom');

var _reactDom2 = _interopRequireDefault(_reactDom);

var _reactLoaderSpinner = require('react-loader-spinner');

var _reactLoaderSpinner2 = _interopRequireDefault(_reactLoaderSpinner);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var LoaderSpinner = function (_React$Component) {
  _inherits(LoaderSpinner, _React$Component);

  function LoaderSpinner(props) {
    _classCallCheck(this, LoaderSpinner);

    var _this = _possibleConstructorReturn(this, (LoaderSpinner.__proto__ || Object.getPrototypeOf(LoaderSpinner)).call(this, props));

    _this.state = {
      spinning: false
    };
    return _this;
  }

  _createClass(LoaderSpinner, [{
    key: 'start',
    value: function start() {
      this.setState({ spinning: true });
    }
  }, {
    key: 'stop',
    value: function stop() {
      this.setState({ spinning: false });
    }
  }, {
    key: 'render',
    value: function render() {
      if (!this.state.spinning) return null;
      return _react2.default.createElement(
        'div',
        {
          style: {
            position: 'fixed',
            bottom: '0',
            left: '0',
            right: '0',
            top: '0',
            opacity: '0.5',
            backgroundColor: '#000',
            zIndex: '9999'
          }
        },
        _react2.default.createElement(
          'div',
          {
            style: {
              position: 'fixed',
              left: '50%',
              top: '50%',
              margin: '-25px',
              zIndex: '100'
            }
          },
          _react2.default.createElement(_reactLoaderSpinner2.default, { type: this.props.type, color: '#FFF', height: '50', width: '50' })
        )
      );
    }
  }]);

  return LoaderSpinner;
}(_react2.default.Component);

exports.default = { start: start, stop: stop };

/**
 *
 * @type {{type: *}}//Audio Ball-Triangle Bars Circles Grid Hearts Oval Puff Rings TailSpin ThreeDots
 */

LoaderSpinner.propTypes = {
  type: _propTypes2.default.string.isRequired
};

LoaderSpinner.defaultProps = {
  type: 'TailSpin'
};

var globalProgress = null;
function initial() {
  var node = document.createElement('DIV');
  node.id = 'Loader-Spinner';
  document.body.appendChild(node);
  _reactDom2.default.render(_react2.default.createElement(LoaderSpinner, { ref: function ref(_ref) {
      globalProgress = _ref;
      // globalProgress.start()
    } }), document.getElementById('Loader-Spinner'));
}
function start(args) {
  if (globalProgress) {
    globalProgress.start(args);
  }
}
function stop() {
  var delay = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

  if (globalProgress) {
    setTimeout(function () {
      globalProgress.stop();
    }, delay);
  }
}