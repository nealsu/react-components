'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 *
 * @param {string} [id] - dom element #id
 * @param {object} [menu] - menu info
 * @param {string} [menuIndex] - now menu index
 * @param {function} [onChange] - handle tab change
 * @param {function} [handleAdd] - handle add while click +
 */

var Nav = function (_Component) {
    _inherits(Nav, _Component);

    function Nav() {
        _classCallCheck(this, Nav);

        return _possibleConstructorReturn(this, (Nav.__proto__ || Object.getPrototypeOf(Nav)).apply(this, arguments));
    }

    _createClass(Nav, [{
        key: 'checkActive',
        //object

        // AddNavTab() {
        //     const {handleAdd} = this.props
        //     handleAdd();
        // }
        //
        // DeleteNavTab(key) {
        //     const {handleDelete} = this.props
        //     handleDelete(key);
        // }

        value: function checkActive(index) {
            if (index === this.props.menuIndex) {
                return 'nav-link active';
            }
            return 'nav-link';
        }
    }, {
        key: 'switchTab',
        value: function switchTab(index) {
            var onChange = this.props.onChange;

            onChange(index);
        }
    }, {
        key: 'renderContent',
        value: function renderContent() {
            var _props = this.props,
                menu = _props.menu,
                menuIndex = _props.menuIndex;

            return menu[menuIndex].content;
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            //TODO remove
            var _props2 = this.props,
                menu = _props2.menu,
                id = _props2.id;

            return _react2.default.createElement(
                'div',
                { id: id },
                _react2.default.createElement(
                    'ul',
                    { className: 'nav nav-tabs' },
                    _lodash2.default.map(menu, function (_ref, key) {
                        var label = _ref.label;
                        return _react2.default.createElement(
                            'li',
                            { className: 'nav-item', key: key },
                            _react2.default.createElement(
                                'div',
                                { className: 'relative' },
                                _react2.default.createElement(
                                    'a',
                                    { className: _this2.checkActive(key), href: 'javascript:void(0);', onClick: function onClick(e) {
                                            return _this2.switchTab(key);
                                        } },
                                    label
                                )
                            )
                        );
                    })
                ),
                _react2.default.createElement('br', null),
                _react2.default.createElement(
                    'div',
                    null,
                    this.renderContent()
                )
            );
        }
    }]);

    return Nav;
}(_react.Component);

exports.default = Nav;