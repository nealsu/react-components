'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _objectPathImmutable = require('object-path-immutable');

var _objectPathImmutable2 = _interopRequireDefault(_objectPathImmutable);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _reactFontawesome = require('@fortawesome/react-fontawesome');

var _reactFontawesome2 = _interopRequireDefault(_reactFontawesome);

var _fontawesomeFreeSolid = require('@fortawesome/fontawesome-free-solid');

var _common = require('./utils/common');

var Common = _interopRequireWildcard(_common);

var _input = require('./input/input');

var _input2 = _interopRequireDefault(_input);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DataTable = function (_React$Component) {
  _inherits(DataTable, _React$Component);

  function DataTable(props) {
    _classCallCheck(this, DataTable);

    var _this = _possibleConstructorReturn(this, (DataTable.__proto__ || Object.getPrototypeOf(DataTable)).call(this, props));

    _this.state = {
      sorts: {}
    };
    return _this;
  }

  _createClass(DataTable, [{
    key: 'editObjectKey',
    value: function editObjectKey(object, oldkey, newkey) {
      var handleChange = this.props.handleChange;

      var newObject = Common.renameObjectKey(object, oldkey, newkey);
      handleChange(newObject);
    }
  }, {
    key: 'addButton',
    value: function addButton() {
      var _this2 = this;

      var addable = this.props.addable;

      if (addable) return _react2.default.createElement(
        'a',
        {
          className: 'cursor-pointer',
          onClick: function onClick() {
            _this2.handleAdd();
          }
        },
        _react2.default.createElement(_reactFontawesome2.default, { icon: _fontawesomeFreeSolid.faPlus, size: 'sm' })
      );
    }
  }, {
    key: 'handleAdd',
    value: function handleAdd() {
      var _props = this.props,
          handleChange = _props.handleChange,
          _props$emptyRow = _props.emptyRow,
          emptyRow = _props$emptyRow === undefined ? {} : _props$emptyRow;
      var rawData = this.props.rawData;

      if (!rawData) rawData = {};
      if (_lodash2.default.isArray(rawData)) rawData = Common.handleArrayAdd(rawData, emptyRow);else rawData = Common.handleObjectAdd(rawData, 'newOne', emptyRow);
      handleChange(rawData);
    }
  }, {
    key: 'handleDelete',
    value: function handleDelete(key) {
      var handleChange = this.props.handleChange;
      var rawData = this.props.rawData;

      if (_lodash2.default.isArray(rawData)) Common.handleArrayDelete(rawData, key);else Common.handleObjectDelete(rawData, key);
      handleChange(rawData);
    }
  }, {
    key: 'renderEditor',
    value: function renderEditor(value, rid, field, fieldsKey) {
      var _props2 = this.props,
          rawData = _props2.rawData,
          handleChange = _props2.handleChange;
      var editor = field.editor,
          title = field.title,
          props = field.props;


      props = _extends({}, field.props, {
        id: fieldsKey + '_' + title,
        value: value,
        onChange: function onChange(value) {
          _lodash2.default.set(rawData[rid], fieldsKey, value);
          handleChange(rawData);
        }
      });
      return _react2.default.createElement(editor, props);
    }
  }, {
    key: 'renderSortButton',
    value: function renderSortButton(key) {
      var _this3 = this;

      var _state$sorts = this.state.sorts,
          sorts = _state$sorts === undefined ? {} : _state$sorts;

      var sort = _lodash2.default.get(sorts, key, 'DESC');
      return _react2.default.createElement(
        'a',
        {
          className: 'cursor-pointer sort-button',
          onClick: function onClick() {
            _this3.onSort(key);
          }
        },
        _react2.default.createElement(_reactFontawesome2.default, {
          icon: sort === 'DESC' ? _fontawesomeFreeSolid.faSortAlphaDown : _fontawesomeFreeSolid.faSortAlphaUp,
          size: 'sm'
        })
      );
    }
  }, {
    key: 'onSort',
    value: function onSort(key) {
      var _state$sorts2 = this.state.sorts,
          sorts = _state$sorts2 === undefined ? {} : _state$sorts2;
      var onSort = this.props.onSort;

      var sort = _lodash2.default.get(sorts, key, 'DESC');
      var sortTo = sort === 'DESC' ? 'ASC' : 'DESC';
      this.setState({
        sorts: _objectPathImmutable2.default.set(sorts, key, sortTo)
      });
      onSort(key, sortTo);
    }
  }, {
    key: 'renderField',
    value: function renderField(field, row, fieldsKey, rowKey) {
      var value = _lodash2.default.get(row, fieldsKey, null);
      if (field.formatter) return _react2.default.createElement(
        'div',
        null,
        field.formatter(value, row, rowKey)
      );
      if (typeof value === 'boolean') value = value.toString();
      var content = null;
      if (field.editor) content = this.renderEditor(value, rowKey, field, fieldsKey);else if (field.format) {
        switch (field.format) {
          case 'date':
            content = (0, _moment2.default)(value).format('YYYY-MM-DD');
            break;
          case 'dateTime':
            content = (0, _moment2.default)(value).format('YYYY-MM-DD HH:mm:ss');
            break;
        }
      } else content = value !== null ? value : '';
      return _react2.default.createElement(
        'div',
        null,
        content
      );
    }
  }, {
    key: 'render',
    value: function render() {
      var _this4 = this;

      var _props3 = this.props,
          id = _props3.id,
          rawData = _props3.rawData,
          fields = _props3.fields,
          _props3$className = _props3.className,
          className = _props3$className === undefined ? 'table-striped table-bordered' : _props3$className,
          _props3$theadClassNam = _props3.theadClassName,
          theadClassName = _props3$theadClassNam === undefined ? 'thead-dark' : _props3$theadClassNam,
          keyTitle = _props3.keyTitle,
          addable = _props3.addable,
          deletable = _props3.deletable;

      var totalFields = _lodash2.default.size(fields);
      if (keyTitle) totalFields++;
      if (deletable) totalFields++;
      return _react2.default.createElement(
        'div',
        null,
        _react2.default.createElement(
          'table',
          { id: 'dataTable-' + id, className: (0, _classnames2.default)('table', className) },
          _react2.default.createElement(
            'thead',
            { className: theadClassName },
            _react2.default.createElement(
              'tr',
              null,
              deletable ? _react2.default.createElement(
                'th',
                null,
                '#'
              ) : null,
              keyTitle ? _react2.default.createElement(
                'th',
                null,
                keyTitle
              ) : null,
              _lodash2.default.map(fields, function (field, key) {
                return _react2.default.createElement(
                  'th',
                  { key: key },
                  field.title,
                  field.sortable ? _this4.renderSortButton(field.sortKey ? field.sortKey : key) : null
                );
              })
            )
          ),
          _react2.default.createElement(
            'tbody',
            null,
            _lodash2.default.isEmpty(rawData) ? _react2.default.createElement(
              'tr',
              null,
              _react2.default.createElement(
                'td',
                { colSpan: totalFields, className: 'tac' },
                '\u7121\u8CC7\u6599'
              )
            ) : null,
            _lodash2.default.map(rawData, function (row, rowKey) {
              return _react2.default.createElement(
                'tr',
                { key: rowKey },
                deletable ? _react2.default.createElement(
                  'td',
                  null,
                  _react2.default.createElement(
                    'div',
                    null,
                    _react2.default.createElement(
                      'a',
                      {
                        className: 'cursor-pointer',
                        onClick: function onClick() {
                          _this4.handleDelete(rowKey);
                        }
                      },
                      'X'
                    )
                  )
                ) : null,
                keyTitle ? _react2.default.createElement(
                  'td',
                  null,
                  addable ? _react2.default.createElement(_input2.default, {
                    id: id + '_' + rowKey + '_Name',
                    placeholder: 'Name',
                    value: rowKey,
                    handleChange: function handleChange(value) {
                      _this4.editObjectKey(rawData, rowKey, value);
                    },
                    validation: ['required'],
                    tooltipPlacement: 'bottom'
                  }) : rowKey
                ) : null,
                _lodash2.default.map(fields, function (field, fieldsKey) {
                  return _react2.default.createElement(
                    'td',
                    { key: fieldsKey },
                    _this4.renderField(field, row, fieldsKey, rowKey)
                  );
                })
              );
            }),
            addable ? _react2.default.createElement(
              'tr',
              null,
              _react2.default.createElement(
                'td',
                { colSpan: totalFields, className: 'tac' },
                this.addButton()
              )
            ) : null
          )
        )
      );
    }
  }]);

  return DataTable;
}(_react2.default.Component);

exports.default = DataTable;


DataTable.propTypes = {
  addable: _propTypes2.default.bool,
  className: _propTypes2.default.string,
  deletable: _propTypes2.default.bool,
  fields: _propTypes2.default.object.isRequired,
  handleChange: _propTypes2.default.func,
  id: _propTypes2.default.string,
  keyTitle: _propTypes2.default.string,
  keyTitleCol: _propTypes2.default.number,
  rawData: _propTypes2.default.any.isRequired
};