'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactAddonsCreateFragment = require('react-addons-create-fragment');

var _reactAddonsCreateFragment2 = _interopRequireDefault(_reactAddonsCreateFragment);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Card = function (_Component) {
  _inherits(Card, _Component);

  function Card(props) {
    _classCallCheck(this, Card);

    var _this = _possibleConstructorReturn(this, (Card.__proto__ || Object.getPrototypeOf(Card)).call(this, props));

    _this.state = {
      formData: {}
    };
    return _this;
  }

  _createClass(Card, [{
    key: 'render',
    value: function render() {
      var _props = this.props,
          children = _props.children,
          header = _props.header,
          footer = _props.footer,
          className = _props.className,
          headerClassName = _props.headerClassName,
          bodyClassName = _props.bodyClassName,
          footerClassName = _props.footerClassName;

      var fragment = {
        header: _react2.default.createElement(
          'div',
          {
            className: (0, _classnames2.default)('card-header', headerClassName ? headerClassName : '')
          },
          header
        ),
        children: _react2.default.createElement(
          'div',
          { className: (0, _classnames2.default)('card-body', bodyClassName ? bodyClassName : '') },
          children
        )
      };
      if (footer) fragment[footer] = _react2.default.createElement(
        'div',
        {
          className: (0, _classnames2.default)('card-footer', footerClassName ? footerClassName : '')
        },
        footer
      );
      return _react2.default.createElement(
        'div',
        { className: (0, _classnames2.default)('card mb-3', className ? className : '') },
        (0, _reactAddonsCreateFragment2.default)(fragment)
      );
    }
  }]);

  return Card;
}(_react.Component);

exports.default = Card;


Card.propTypes = {
  children: _propTypes2.default.any.isRequired,
  footer: _propTypes2.default.any,
  header: _propTypes2.default.any.isRequired
};