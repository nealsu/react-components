'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InputType = exports.Form = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _lodash = require('lodash');

var _lodash2 = _interopRequireDefault(_lodash);

var _objectPathImmutable = require('object-path-immutable');

var _objectPathImmutable2 = _interopRequireDefault(_objectPathImmutable);

var _input = require('./input');

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var InputType = {
  INPUT: 'Input',
  DATE_RANGE: 'DateRange',
  TEXT_AREA: 'TextArea',
  DROP_DOWN_LIST: 'DropDownList',
  CHECK_BOX: 'CheckBox'
};

var Form = function (_React$Component) {
  _inherits(Form, _React$Component);

  function Form(props) {
    _classCallCheck(this, Form);

    var _this = _possibleConstructorReturn(this, (Form.__proto__ || Object.getPrototypeOf(Form)).call(this, props));

    _this.state = {
      // formData: this.props.data
    };
    return _this;
  }

  _createClass(Form, [{
    key: 'renderInner',
    value: function renderInner(field, key) {
      var _this2 = this;

      var data = this.props.data;

      var value = _lodash2.default.get(data, key, '');
      if (_lodash2.default.has(field, 'editor')) {
        var props = _extends({
          id: 'input-' + key,
          value: value,
          onChange: function onChange(value) {
            _this2.onChange(key, value);
          }
        }, field.props);
        return _react2.default.createElement(
          'div',
          { id: key, className: 'form-group' },
          _react2.default.createElement(
            'label',
            { htmlFor: key },
            field.title
          ),
          _react2.default.createElement(field.editor, props)
        );
      }
      switch (field.inputType) {
        case InputType.INPUT:
        default:
          return _react2.default.createElement(_input.FormInput, {
            id: key,
            label: field.title,
            placeholder: field.placeholder ? field.placeholder : field.title,
            value: value,
            onChange: function onChange(value) {
              _this2.onChange(key, value);
            }
          });
        case InputType.DATE_RANGE:
          return _react2.default.createElement(_input.FormDateRange, {
            id: key,
            label: field.title,
            value: value,
            onChange: function onChange(value) {
              _this2.onChange(key, value);
            }
          });
      }
    }
  }, {
    key: 'onChange',
    value: function onChange(key, value) {
      var onChange = this.props.onChange;
      var data = this.props.data;

      var tempFormData = _objectPathImmutable2.default.set(data, key, value);
      // this.setState({data: tempFormData})
      onChange(tempFormData);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this3 = this;

      var _props = this.props,
          fields = _props.fields,
          _props$columnCount = _props.columnCount,
          columnCount = _props$columnCount === undefined ? 1 : _props$columnCount;

      var bootstrapCol = 12 / columnCount;
      return _react2.default.createElement(
        'form',
        { className: (0, _classnames2.default)(columnCount !== 1 ? 'row' : '') },
        _lodash2.default.map(fields, function (field, key) {
          return _react2.default.createElement(
            'div',
            {
              key: key,
              className: (0, _classnames2.default)(columnCount !== 1 ? 'col-' + bootstrapCol : '')
            },
            _this3.renderInner(field, key)
          );
        })
      );
    }
  }]);

  return Form;
}(_react2.default.Component);

exports.Form = Form;
exports.InputType = InputType;


Form.propTypes = {
  data: _propTypes2.default.object.isRequired,
  fields: _propTypes2.default.object.isRequired,
  onChange: _propTypes2.default.func.isRequired
};